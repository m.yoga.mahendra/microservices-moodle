# stacks
Provisioning scripts for Datastream layers with Docker-compose. These containers are equipped with autorestart.

## Setting Datastream Up
Prerequisites: Docker is installed, and Portainer is running using `./run_portainer.sh`
1. Make sure `warp` is running (https://gitlab.cs.ui.ac.id/csui-datastream/warp)
2. Setup Debezium docker in `debeziun-infra/`
3. Setup Elasticsearch docker in `elastic/`
4. Setup Influx docker in `influx/`
5. Setup Flink Cluster in `flink-cluster/`
6. Monitoring stacks and Grafana comes last. Setup the cluster in `monitoring/`

## Notes
Please add to help other developers
1. [FAVIAN] Flink cluster task managers can be scaled with 
`docker-compose scale taskmanager=<NUM>`
2. [FAVIAN] For Elasticsearch cluster set this in host server:
```
grep vm.max_map_count /etc/sysctl.conf
vm.max_map_count=262144
```
