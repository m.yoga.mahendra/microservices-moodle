#!/bin/bash

SERVER=`hostname -I | cut -d' ' -f1`

curl --noproxy "*" -i -X DELETE http://$SERVER:8083/connectors/p-scele-connector-log \
	        -H "Accept:application/json"

curl --location --request POST "http://$SERVER:8083/connectors/" \
	--header 'Content-Type: application/json' \
	--data-raw '{
	    "name": "p-scele-connector-log",
	    "config": {
		"connector.class": "io.debezium.connector.postgresql.PostgresConnector",
		"database.user": "moodleuser",
		"database.dbname": "postgres",
		"database.hostname": "'$SERVER'",
		"database.password": "moodleuser",
		"name": "p-scele-connector-log",
		"database.server.name": "p-scele",
		"database.port": "5431",
		"table.whitelist": "public.mdl_logstore_standard_log,public.mdl_course,public.mdl_user,public.mdl_asynchronous",
		"plugin.name": "decoderbufs",
		"snapshot.mode": "never",
		"slot.name": "dbzlog5"
	    }
	}' 

