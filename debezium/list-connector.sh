#!/bin/bash

SERVER=`hostname -I | cut -d' ' -f1`

curl --noproxy "*" -i -X GET $SERVER:8083/connectors/ \
	        -H "Accept:application/json"

